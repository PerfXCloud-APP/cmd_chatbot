from openai import OpenAI

client = OpenAI(
    base_url='https://perfxchat.com:1443/v1',
    api_key="sk-wp5aWhYfAbCkHkKSCa42358d07Ef4c3a84E63431B22b840c"
)

stream = client.chat.completions.create(
    model="chatglm3-6b",
    messages=[{"role": "user",
               "content": "hello "}],
    temperature=1,
    max_tokens=16,
    n=1,
    presence_penalty=0,
    frequency_penalty=0,
    stream=True,
)

for chunk in stream:
    if chunk.choices[0].delta.content is not None:
        print(chunk.choices[0].delta.content, end="")
