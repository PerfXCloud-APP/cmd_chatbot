from openai import OpenAI

client = OpenAI(
    base_url='https://perfxchat.com:1443/v1',
    api_key='sk-wp5aWhYfAbCkHkKSCa42358d07Ef4c3a84E63431B22b840c'
)


def main():
    # 开始对话，并存储对话历史，以便进行上下文管理
    conversation_history = []

    while True:
        # 获取用户输入
        user_input = input("您: ")
        # 将用户输入添加到对话历史
        conversation_history.append({"role": "user", "content": user_input})

        # 调用API进行对话
        stream = client.chat.completions.create(
            model="chatglm3-6b",
            messages=conversation_history,
            temperature=1,
            max_tokens=512,
            n=1,
            presence_penalty=0,
            frequency_penalty=0,
            stream=True,
        )

        print("AI: ", end="")
        assistants_response = ""
        for chunk in stream:
            if chunk.choices[0].delta.content is not None:
                assistants_response += chunk.choices[0].delta.content
                print(chunk.choices[0].delta.content, end="")

        print()
        # 将机器人回复添加到对话历史
        conversation_history.append({"role": "assistant", "content": assistants_response})

        # 检查是否结束对话
        if "再见" in user_input or "退出" in user_input:
            break


if __name__ == "__main__":
    main()
